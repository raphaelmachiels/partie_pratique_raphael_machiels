'use strict';

var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var autoprefix = require('gulp-autoprefixer');
var csso = require('gulp-csso');


gulp.task('copy', function() {
    gulp.src([
        'src/*',
        '!src/scss/',
    ], {
        dot: true,
    })
        .pipe(gulp.dest('dist'));
});

gulp.task('style', function() {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefix({
            browsers: ['last 2 versions']
        }))
        .pipe(csso())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist'));
});

gulp.task('default', ['style', 'copy']);